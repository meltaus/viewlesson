package com.viewlessens.javafx.ru.object;

import javafx.beans.property.SimpleStringProperty;

//Объект для хранения списка уроков
public class Lesson {
    private SimpleStringProperty lesson = new SimpleStringProperty("");

    public Lesson() {

    }

    //Печатает содержимое объекта в консоль
    @Override
    public String toString() {
        return "Lesson{" +
                "lesson=" + lesson +
                '}';
    }

    public Lesson(String lesson) {
        this.lesson = new SimpleStringProperty (lesson);
    }

    public String getLesson() {
        return lesson.get();
    }

    public SimpleStringProperty lessonProperty() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson.set(lesson);
    }
}
