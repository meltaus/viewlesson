package com.viewlessens.javafx.ru.object;

import java.io.*; //Библиотека I/O

public class FileConfig {

    //Путь к каталогу с уроками
    private String pathlessens;

    private static String fileconfig = "config.txt";
    public String getPathlessens() {
        return pathlessens;
    }

    //Установка пути с .html файлами
    public void setPathlessens(String pathlessens) {
        writeFile(pathlessens);
        this.pathlessens = pathlessens;
    }

    //Загружает файл настроек
    public void LoadConfig() {
        try {
            //Открываем файл для считывания настроек (одна строка). В случае отсутствия создаем файл и щаписываем туда None
            File file = new File(fileconfig);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String inputFile = bufferedReader.readLine();

            this.pathlessens = inputFile;
        } catch (FileNotFoundException e) {
            CreateFileConfig();
            writeFile("None");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //Записываем в файл хоть что-то
    private void writeFile(String data) {
        OutputStream os = null;
        try {
            os = new FileOutputStream(fileconfig);
            os.write(data.getBytes(), 0, data.length());
            this.pathlessens = data;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    //Создает файл конфигурации
    private static void CreateFileConfig() {
        File newFile = new File(fileconfig);
        try {
            boolean created = newFile.createNewFile();
            if (created) {
                System.out.println("Файл создан");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
