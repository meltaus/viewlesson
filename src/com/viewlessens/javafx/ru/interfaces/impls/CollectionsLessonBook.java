package com.viewlessens.javafx.ru.interfaces.impls;

import com.viewlessens.javafx.ru.interfaces.LessonBook;
import com.viewlessens.javafx.ru.object.Lesson;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

//Реализация интерфейса LessonBook

public class CollectionsLessonBook implements LessonBook {

    private ObservableList<Lesson> lessonsList = FXCollections.observableArrayList();

    @Override
    public void add(Lesson lesson) {
        lessonsList.add(lesson);
    }

    @Override
    public void update(Lesson lesson) {

    }

    @Override
    public void delete(Lesson lesson) {
        lessonsList.remove(lesson);
    }

    public ObservableList<Lesson> getLessonsList() {
        return lessonsList;
    }

    //Выводит в консоль содержимое коллекции. Для отладки
    public void print(){
        int number = 0;
        for (Lesson lesson : lessonsList) {
            number++;
            System.out.println(number + ") lesson " + lesson.getLesson());
        }

    }
}
