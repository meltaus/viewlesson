package com.viewlessens.javafx.ru.interfaces;

import com.viewlessens.javafx.ru.object.Lesson;

public interface LessonBook {

    void add(Lesson lesson);

    void update(Lesson lesson);

    void delete(Lesson lesson);
}
