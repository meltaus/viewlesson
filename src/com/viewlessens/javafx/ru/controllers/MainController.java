package com.viewlessens.javafx.ru.controllers;

import com.viewlessens.javafx.ru.interfaces.impls.CollectionsLessonBook; //Контроллер
import com.viewlessens.javafx.ru.object.FileConfig; //Файл конфигурации
import com.viewlessens.javafx.ru.object.Lesson; //Объект Lesson
import javafx.event.ActionEvent;    //Работа с fxml
import javafx.event.EventHandler;   //О
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.*;                   //Операции I/O
import java.nio.file.Files;         //Операции над файлами и каталогами

public class MainController {

    //Файл для открытия
    private String location = new String();

    //Коллекция уроков
    CollectionsLessonBook lessonBookimpl = new CollectionsLessonBook();

    private Stage mainStage;

    //Работа с файлом конфигурации
    FileConfig fileConfig = new FileConfig();
    //Инициализация WebView, облась отображения уроков
    @FXML
    private WebView browser;
    private WebEngine webEngine;

    //Инициализация HTMLEditor
    private final HTMLEditor htmlEditor = new HTMLEditor();

    //Инициализация элементов макета
    @FXML
    private AnchorPane anchorHTMLEditor;
    @FXML
    private TableView tableLessView;
    @FXML
    private TableColumn<Lesson, String> columnLesson;
    @FXML
    private TextArea statusBar;
    @FXML
    private Button btnGoToFirstAction;
    @FXML
    private Button btnGoToPrevious;
    @FXML
    private Button btnGoToNext;
    @FXML
    private Button btnGoToLast;

    //Метод, выполняемый при инициализации макета
    @FXML
    private void initialize() {
        //Инициализируем TableColum
        columnLesson.setCellValueFactory(new PropertyValueFactory<Lesson, String>("lesson"));

        updateListLeson();
        //Заполняем TableView
        tableLessView.setItems(lessonBookimpl.getLessonsList());
        showWebView();
        getListLessonOnDirecory();
        //Инициализиуем webViewer
        webEngine = browser.getEngine();
        webEngine.setJavaScriptEnabled(true);

        initListeners();
    }

    //Выход
    public void onExit() {
        System.exit(0);
    }

    //Слушатели
    private void initListeners() {
        //Двойной клик мыши по элементу списка
        tableLessView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    //Открытие выбранного элемента
                    Lesson templesson = (Lesson) tableLessView.getSelectionModel().getSelectedItem();
                    showWebView(templesson);
                }
            }
        });
    }
    //Отображает элемент WebView
    private void showWebView () {
        webEngine = browser.getEngine();
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }
    //Настройка DirectoryChooser
    private void configuringDirectoryChooser(DirectoryChooser directoryChooser) {
        // Set title for DirectoryChooser
        directoryChooser.setTitle("Выберите дирректорию с уроками");

        // Set Initial Directory
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));
    }
    //Настройка FileChooser
    private void chooserFileLesson(FileChooser fileChooser) {
        // Set title for DirectoryChooser
        fileChooser.setTitle("Выберите файл урока");

        // Set Initial Directory
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("HTML", "*.html")
        );
    }

    //Получает список уроков из директории
    private void getListLessonOnDirecory () {
        //Выбираем рабочий каталог
        File dir = new File(fileConfig.getPathlessens());
        if(dir.isDirectory()) {
            for (File item : dir.listFiles()) {
                if(item.isFile()) {
                    if (item.getName().contains(".html")) {
                        String s = item.getName();
                        s = s.replaceAll(".html", "");
                        lessonBookimpl.add(new Lesson(s));
                    }
                }
            }
        }
    }

    //Добавление урока
    public void addLesson() {
        try {
            FileChooser fileChooser = new FileChooser();
            chooserFileLesson(fileChooser);
            File selectedFile = fileChooser.showOpenDialog(mainStage);
            File pastFaile = new File(fileConfig.getPathlessens() + "/" + selectedFile.getName());
            Files.copy(selectedFile.toPath(),pastFaile.toPath());
            String sourceName = selectedFile.getName();
            sourceName = sourceName.replace(".html","");
            lessonBookimpl.add(new Lesson(sourceName));
            String sourceDir = selectedFile.getPath();
            sourceDir = sourceDir.replace(".html","");
            sourceDir += "_files";
            File srcDir = new File(sourceDir);
            File destDir = new File(fileConfig.getPathlessens() + "/" + sourceName);
            try {
                copyFolder(srcDir, destDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(sourceDir);
            System.out.print("\n" + fileConfig.getPathlessens() + "/" + sourceName);
            tableLessView.refresh();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //Копируем каталог
    public static void copyFolder(File src, File dest)
            throws IOException{

        if(src.isDirectory()){

            //if directory not exists, create it
            if(!dest.exists()){
                dest.mkdir();
                System.out.println("Directory copied from "
                        + src + "  to " + dest);
            }

            //list all the directory contents
            String files[] = src.list();

            for (String file : files) {
                //construct the src and dest file structure
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                //recursive copy
                copyFolder(srcFile,destFile);
            }

        }else{
            //if file, then copy it
            //Use bytes stream to support all file types
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);

            byte[] buffer = new byte[1024];

            int length;
            //copy the file content in bytes
            while ((length = in.read(buffer)) > 0){
                out.write(buffer, 0, length);
            }

            in.close();
            out.close();
            System.out.println("File copied from " + src + " to " + dest);
        }
    }
    //Обновляем список уроков
    private void updateListLeson() {
        //Проверяем, задана ли дирректория с уроками
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        configuringDirectoryChooser(directoryChooser);
        fileConfig.LoadConfig();
        if (fileConfig.getPathlessens().equals("None")) {
            File dir = directoryChooser.showDialog(mainStage);
            if (dir != null) {
                fileConfig.setPathlessens(dir.getAbsolutePath());
            } else {
                updateListLeson();
            }
        }
    }
    //Отправляем файл на WebView
    private void showWebView(Lesson lesson) {
        //Собираем ссылку для WebView
        String filename = "//" + lesson.getLesson() + ".html";
        location = "file:" + fileConfig.getPathlessens() + filename;
        statusBar.setText("Вы открыли урок " + lesson.getLesson());
        webEngine.load(location);
    }
    //Обработка навигационных кнопок
    public void actionNavigationButtons(ActionEvent actionEvent) {
        //Получаем нажатую кнопка и проверяем ее статус
        Object source = actionEvent.getSource();

        //Если кнопка не нажата - выходит из метода
        if (!(source instanceof Button)) {
            return;
        }

        Button clickedButton = (Button) source;

        //Обрабатываем нажатие кнопок по ID
        switch (clickedButton.getId()) {
            case "btnGoToFirstAction":
                tableLessView.getSelectionModel().selectFirst();
                break;
            case "btnGoToPrevious":
                tableLessView.getSelectionModel().selectPrevious();
                break;
            case "btnGoToNext":
                tableLessView.getSelectionModel().selectNext();
                break;
            case "btnGoToLast":
                tableLessView.getSelectionModel().selectLast();
                break;
        }
        //Обновляем список
        Lesson templesson = (Lesson) tableLessView.getSelectionModel().getSelectedItem();
        showWebView(templesson);

    }
    //О программе
    public void showAbout() {
        webEngine.loadContent("<br>Данная программа предназначена для демонстрации методов вычисления интегралов.</br>\n" +
                "<br>Что бы добавить метод нажмите кнопку \"Добавить метод\" и выберите файл .html</br>\n" +
                "</br>Вместе с ним загрузится папка с аналогичным названием и припиской</br>", "text/html");
    }
    //Удаляет файл, папку и запись
    public void deleteLesson() {
        Lesson templesson = (Lesson)tableLessView.getSelectionModel().getSelectedItem();
        //Удаляем файл
        File file = new File(fileConfig.getPathlessens() + "/" + templesson.getLesson() + ".html");
        file.delete();
        //Удаляем папку
        file = new File(fileConfig.getPathlessens() + "/" + templesson.getLesson() + "_files");
        file.delete();
        //Удаляем элемент коллекции
        lessonBookimpl.delete(templesson);
    }
}
