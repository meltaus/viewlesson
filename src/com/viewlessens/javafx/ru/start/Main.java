package com.viewlessens.javafx.ru.start;

import com.viewlessens.javafx.ru.controllers.MainController; //Контроллер

import javafx.application.Application;
import javafx.fxml.FXMLLoader;  //Работа с fxml
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        //Инициализация главного окна, макета и контроллера
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("../fxml/main.fxml"));
        Parent fxmlMain = fxmlLoader.load();
        MainController mainController = fxmlLoader.getController();
        mainController.setMainStage(primaryStage);

        //Минимальный размер окна
        primaryStage.setTitle("Справочник. Решение интегральных уравнений различными методами");
        primaryStage.setMinHeight(450);
        primaryStage.setMinWidth(600);
        primaryStage.setScene(new Scene(fxmlMain, 300, 275));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
